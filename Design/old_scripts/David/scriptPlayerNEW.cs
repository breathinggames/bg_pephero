﻿using UnityEngine;
using System.Collections;

public class scriptPlayerNEW : MonoBehaviour {

	public float pressurePosition;
	public float pressureFixed = 0.55f;
	public float pressureBuffer = 0.01f;
	public float pressureMultiplier = 2.0f;
	public float minYPosition = 0f;
	
	// Use this for initialization
	void Start () {
		//readSerialPort sp = readSerialPort.GetSceneInstance ();

	}
	
	// Update is called once per frame
	void Update () {

		//pressurePosition = sp.pressure;

		readSerialPort sp = readSerialPort.GetSceneInstance ();
		pressurePosition = sp.pressure;
		if (Mathf.Abs (pressurePosition - pressureFixed) > pressureBuffer){
			Vector3 newPlayerPosition = transform.position;
			newPlayerPosition = new Vector3(transform.position.x, (pressurePosition - pressureFixed) * pressureMultiplier, transform.position.z);		//sets horizontal and vertical limit
			transform.position = newPlayerPosition;
		}
		if (Mathf.Abs (pressurePosition - pressureFixed) < pressureBuffer){
			Vector3 newPlayerPosition = transform.position;
			newPlayerPosition = new Vector3(transform.position.x, minYPosition, transform.position.z);		//sets horizontal and vertical limit
			transform.position = newPlayerPosition;
		}

		//float playerYPosition = transform.position.y;

		

//		if (Input.GetAxis("Vertical"){
//
//		}
	}
}
