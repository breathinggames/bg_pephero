﻿using UnityEngine;
using System.Collections;

public class js_Shield : MonoBehaviour {

	GameObject shield;
	GameObject waveSpawn;
	Waves waveScript;
	float pressurePosit;

	void Start () {

		waveSpawn = GameObject.Find ("waveSpawner");
		waveScript = waveSpawn.GetComponent<Waves> ();
		shield = GameObject.FindWithTag("Shield");
		shield.SetActive(false);

	}

	void Update () {
		
		readSerialPort sp = readSerialPort.GetSceneInstance ();
		pressurePosit = sp.pressure;
		if(waveScript.WaveType==3){
			if (pressurePosit<15&&pressurePosit>5){
				shield.SetActive(true);
			}
		}
	
	}
	void OnCollisionEnter(Collision col){
		if (col.gameObject.tag == "Enemy"&&shield.activeSelf) {
			waveScript.killObject(col.gameObject);
		}
	}
}