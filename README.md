# PEP Hero
A game for health co-created by commonners of [Breathing Games](http://www.breathinggames.net), and released under free/libre licence.

Developed on [Unity 3D](http://unity3d.com), to be used on a [Firefox](https://www.mozilla.org/en-US/firefox/products/) browser or [Android](https://www.android.com) phone, with a [breathing device](https://gitlab.com/breathinggames/bg/wikis/5-hardware).

**Read the [documentation](https://gitlab.com/breathinggames/bg_pephero/wikis/home).**


## In short
A prototype which focuses on teaching the kid to inhale and exhale constantly and during the correct time.


## Contributors
Contributors are welcome. To join, contact info (at) breathinggames.net